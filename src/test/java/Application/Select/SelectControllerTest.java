package Application.Select;

import Application.Main.MainWindowController;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class SelectControllerTest {
    SelectController controller = new SelectController();

    @Test
    void setController() {
        MainWindowController mainWindowController = new MainWindowController();
        controller.setController(mainWindowController);

        assertEquals(mainWindowController, controller.getController());
    }

    @Test
    void setType() {
        String s = "open";
        controller.setType(s);

        assertEquals(s, controller.getType());
    }

    @Test
    void getController() {
        setController();
    }

    @Test
    void getType() {
        setType();
    }

    /*@Test
    void open() {
        MainWindowController mainWindowController = new MainWindowController();
        mainWindowController.setFilepath("test.txt");
        mainWindowController.initialize();

        controller.setController(mainWindowController);

        Vector<HashMap<String, String>> schedule = mainWindowController.getSchedule();
        schedule.elementAt(0).put("Понедельник", "УППРПО");
        schedule.elementAt(3).put("Вторник", "БД");

        Vector<HashMap<String, String>> result = new Vector<HashMap<String, String>>();
        for (int i = 0; i < 7; ++i)
            result.addElement(new HashMap<String, String>());
        result.elementAt(1).put("Понедельник", "ОТУ");
        result.elementAt(4).put("Среда", "Теория автоматов");

        try {
            String s = "0\n1\nПонедельник ОТУ\n2\n3\n4\nСреда Теория автоматов\n5\n6\n";

            FileWriter writer = new FileWriter("test.txt", false);
            writer.write(s);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        controller.open("test.txt");

        assertEquals(result.elementAt(1).get("Понедельник"), mainWindowController.getSchedule().elementAt(1).get("Понедельник"));
        assertEquals(result.elementAt(4).get("Среда"), schedule.elementAt(4).get("Среда"));

        new File("test.txt").delete();
    }*/

    @Test
    void saveAs() {
        controller.saveAs("test.txt");

        try {
            BufferedReader reader = new BufferedReader(new FileReader("test.txt"));

            String s = "", s1;
            while ((s1 = reader.readLine()) != null)
                s += s1;
            reader.close();

            assertEquals("0123456", s);

            new File("test.txt").delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}