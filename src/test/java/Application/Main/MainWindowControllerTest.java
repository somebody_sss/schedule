package Application.Main;

import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class MainWindowControllerTest {
    MainWindowController controller = new MainWindowController();

    @Test
    void saveOnAction() {
        controller.setFilepath("test.txt");
        controller.initialize();

        controller.SaveOnAction(new ActionEvent());
        try {
            BufferedReader reader = new BufferedReader(new FileReader("test.txt"));
            String s = "", s1;
            while ((s1 = reader.readLine()) != null)
                s += s1;

            reader.close();

            assertEquals("0123456", s);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new File("test.txt").delete();
    }

    @Test
    void setFilepath() {
        String s = "filepath.txt";
        controller.setFilepath(s);

        assertEquals(s, controller.filepath);
    }

    @Test
    void getSchedule() {
        setSchedule();
    }

    @Test
    void setSchedule() {
        Vector<HashMap<String, String>> schedule = new Vector<HashMap<String, String>>();
        for (int i = 0; i < 7; ++i)
            schedule.addElement(new HashMap<String, String>());
        schedule.elementAt(0).put("Понедельник", "УППРПО");

        controller.setSchedule(schedule);

        assertEquals((Object) schedule, (Object) controller.getSchedule());
    }
}